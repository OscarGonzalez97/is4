<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta name="description" content="Mi primer Script PHP">
		<title> Mi Primer Script Php </title>
	</head>
	<body>
		<table>
  			<caption style="background-color: yellow;border: 1px solid black;">Productos</caption>
  			<tr>
    			<th>Nombre</th>
    			<th>Cantidad</th>
    			<th>Precio (Gs.)</th>
  			</tr>
  			<?php
// Enter your code here, enjoy!
$array = array(
	array("Coca Cola", "100", "4.500"),
	array("Pepsi", "100", "4.500"),
	array("Sprite", "100", "4.500"),
	array("Guarana", "100", "4.500"),
	array("Seven Up", "100", "4.500"),
	array("Mirinda naranja", "100", "4.500"),
	array("Mirinda guarana", "100", "4.500"),
	array("Fanta guarana", "100", "4.500"),
	array("Fanta piña", "100", "4.500"),
);

foreach( $array as $key => $value ){
	//echo $key."\t=>\t".$value[0]."\n";
	if($key % 2 == 0){
		echo "<tr style=\"background-color: gray;\">
                                <td>".$value[0]."</td>
                                <td>".$value[1]."</td>
                                <td>".$value[2]."</td>
                        </tr>";

	} else {
		echo "<tr style=\"background-color: #add8e6;\">
                                <td>".$value[0]."</td>
                                <td>".$value[1]."</td>
                                <td>".$value[2]."</td>
                        </tr>";
	}
    
}?>

		</table>
	</body>
</html>
